#scala compared to java tutorial 
    http://docs.scala-lang.org/tutorials/scala-for-java-programmers.html
===========
#install scala and sbt in mac or linux
    http://biercoff.com/easy-steps-to-install-scala-and-sbt-on-your-mac/

-=============
create a build.sbt file and then 'run sbt' == it will download all the dependencies
refer this tutorial (basics no hifi )
http://alvinalexander.com/scala/simple-scala-akka-actor-examples-hello-world-actors
=============

http://rerun.me/2014/09/19/akka-notes-actor-messaging-1/

ActorSystem is the entry point into the ActorWorld. ActorSystems are through which you could create and stop Actors. Or even shutdown the entire Actor environment.
When you create an Actor using the ActorSystem's actorOf method, you create an Actor just below the ActorSystem.

#responsive 
     respond in timely manner 
#resilience 
    -- stay responsive even in failure
#elasticity 
    -- stay responsive in any situation ( 10 ppl or 10000 ppl)
message driven -- 

#distributed system 
    syncing data in every connected system 
    #need
        Availability -- providing resilience when one node fails
        Scalability  -- Increasing load on single node
    problem is while syncing if network gets disconnected
    

    partition for scale
    replicate for availability 

#Akka intro
    ActorRef represents the address of the actor ans enable the actor to send the message to other actor async.

    By default each actor has a dispatcher which enques and dequeues the message received and sent(sent is like fire and forget i.e dont wait for the reply ) 

    we exxtends Actor which encapsulates the state and behaviour of the actor

#Implementing Actor
    In akka an actor is a class which inherits Actor class .

      class PubSubMediator extends Actor {
      override def receive = Actor.emptyBehavior
    }

    receive method returns the nitial behaviour of actor // not getting the feel of it hahaha, thats simply a partial function ( PartialFunction[Any, Unit])to handle the mesaage revd by the actor 

#Actor System and creating actor
    when we create an actor using ActorSystem , Akka creates 3 Actors
    1. root 2. user 3. system guardian
    the user guardian is the parent of the top level actor (these are parent for the child actors which are further created)

    val system = ActorSystem("pub-sub-mediator-spec-system")

    creating top level actor:
    system.actorOf(Props(new PubSubMediator), "pub-sub-mediator")

    actorOf doesnt return Actor instance intead it returns ActorRef
    The name which we proovide must be uniwue else err will be thrown

    here Props is configuration object for the actor that take the constructor as a param




#Akka clustering
    To enable cluster capabilities in your Akka project you should, at a minimum, add the remote settings, and use akka.cluster.ClusterActorRefProvider.

    seed-nodes are the pre configured node to which the new nodes tries to connect to join the cluster,
    Note -- to connect to the remote node ( or another machine ) one has to add the address of the machine instead of localhost(127.0.0.1)






