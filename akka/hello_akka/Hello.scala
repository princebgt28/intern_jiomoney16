import akka.actor.Actor
import akka.actor.ActorSystem
import akka.actor.Props

class HelloActor extends Actor {
  def receive = {
    case "hello" => println("hello back at you")
    case _       => println("huh?")
  }
} 

object Main extends App {
  val system = ActorSystem("HelloSystem")   ///creating root actor 
  // default Actor constructor
  ////When you create an Actor using the ActorSystem's actorOf method, you create an Actor just below the ActorSystem.
  val helloActor = system.actorOf(Props[HelloActor], name = "helloactor")  // this is child actor of system (actor)
  helloActor ! "hello"           /// '!'this sends message "hello"  to the actor 'helloActor'
  helloActor ! "blah"
}
